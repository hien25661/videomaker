package com.zodiac.videomaker.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.effect.EffectFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;

import com.zodiac.videomaker.helpers.EventBusHelper;

import org.greenrobot.eventbus.Subscribe;

import butterknife.ButterKnife;

/**
 * Created by SF on 11/05/2016.
 */
public abstract class BaseActivity extends AppCompatActivity {

    ProgressDialog dialogLoading;

    boolean isUnregistEventBus = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        onPreSetContentView(savedInstanceState);
        super.onCreate(savedInstanceState);
        setContentView(setContentViewId());
        // init actionbar
        ButterKnife.bind(this);
        EventBusHelper.register(this);
        isUnregistEventBus = false;
        initView();
        initDialogApi();
        initData();
    }

    @Subscribe
    public void sampleMethodPreventEventbusCrash(EffectFactory event) {

    }

    private void initDialogApi() {
        dialogLoading = new ProgressDialog(this);
    }

    protected boolean checkApiDialogIsShow() {
        return dialogLoading.isShowing();
    }

    @Override
    protected void onDestroy() {
        if (!isUnregistEventBus) {
            EventBusHelper.unregister(this);
        }
        super.onDestroy();
    }


    public void showLoading() {
        if (dialogLoading != null && !checkApiDialogIsShow()) {
            dialogLoading.show();
        }
    }

    public void dismissLoading() {
        if (dialogLoading != null && checkApiDialogIsShow()) {
            dialogLoading.dismiss();
        }
    }

    private void showApiDialog(ProgressDialog dialogLoading) {
        if (dialogLoading != null && !checkApiDialogIsShow()) {
            dialogLoading.show();
        }
    }

    /**
     * Handle data before setContentView call
     *
     * @param savedInstanceState
     */
    protected void onPreSetContentView(Bundle savedInstanceState) {

    }

    /**
     * @return layout of activity
     */
    public abstract int setContentViewId();

    /**
     * Define your view
     */
    public abstract void initView();

    /**
     * Setup your data
     */
    public abstract void initData();


    @Override
    public void startActivity(Intent intent) {
        EventBusHelper.unregister(this);
        isUnregistEventBus = true;
        super.startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MenuActivity.mCountAds++;
        Log.e("mCountAds: ", " " + MenuActivity.mCountAds);
        if (isUnregistEventBus) {
            EventBusHelper.register(this);
            isUnregistEventBus = false;
        }
    }

}


