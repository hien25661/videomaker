package com.zodiac.videomaker.activities;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.zodiac.videomaker.adapters.VideoCursorAdapter;
import com.zodiac.videomaker.adapters.VideoListAdapter;
import com.zodiac.videomaker.helpers.ChooseVideoEvent;
import com.zodiac.videomaker.models.Media;
import com.zodiac.videomusicmaker.R;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.Bind;

import static com.zodiac.videomaker.activities.MenuActivity.mCountAds;

/**
 * Created by Hien on 6/11/2016.
 */
public class WatchVideoActivity extends BaseActivity {
    AdView adView;
    @Bind(R.id.rootRl)
    RelativeLayout rlRoot;
    @Bind(R.id.PhoneVideoList)
    RecyclerView rcvData;
    ArrayList<Media> mediaArrayList = new ArrayList<>();
    VideoListAdapter adapter;

    public void loadAds() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView = new AdView(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlRoot.addView(adView, params);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(getString(R.string.ads_banner));
        adView.loadAd(adRequest);
    }

    Media media;

    @Override
    public void onResume() {
        super.onResume();
        String[] parameters = {MediaStore.Video.Media._ID,
                MediaStore.Video.Media.DATA,
                MediaStore.Video.Media.DISPLAY_NAME,
                MediaStore.Video.Media.SIZE, MediaStore.Video.Media.DURATION,
                MediaStore.Video.Media.DATE_ADDED,
        };
        Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        videocursor = getContentResolver().query(uri, parameters, null, null, MediaStore.Video.Media.DATE_TAKEN
                + " DESC");
        mediaArrayList.clear();
        for (videocursor.moveToFirst(); !videocursor.isAfterLast(); videocursor.moveToNext()) {
            // The Cursor is now set to the right position
            int fileNameIndex = videocursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            final String filename = videocursor.getString(fileNameIndex);
            Bitmap thumbnail = MediaStore.Video.Thumbnails.getThumbnail(getContentResolver(),
                    getInt(videocursor, MediaStore.Video.Media._ID), MediaStore.Video.Thumbnails.MICRO_KIND, new BitmapFactory.Options());
            media = new Media(filename);
            if (thumbnail != null) {
                media.setWidth(thumbnail.getWidth());
                media.setHeight(thumbnail.getHeight());
                if (!thumbnail.isRecycled()) {
                    thumbnail.recycle();
                }
            }

            MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
            metaRetriever.setDataSource(filename);
            String mHeight = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
            String mWidth = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);

            if (mHeight != null && mWidth != null) {
                try {
                    media.setWidth(Integer.parseInt(mWidth));
                    media.setHeight(Integer.parseInt(mHeight));
                } catch (Exception ex) {

                }
            }
            mediaArrayList.add(media);
        }
        Log.e("Video AAA", "aaaa : " + mediaArrayList.size());
        for (Media a : mediaArrayList) {
            Log.e("Video AAA", "aaaa : " + a.getUrl());
        }
        adapter = new VideoListAdapter(mediaArrayList);
        rcvData.setAdapter(adapter);
//        videocursor = managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
//                parameters, null, null,MediaStore.Video.Media.DATE_TAKEN
//                        + " DESC" );

/*        videolist = (ListView) findViewById(R.id.PhoneVideoList);

        ListAdapter resourceCursorAdapter = new VideoCursorAdapter(this, this, R.layout.video_preview, videocursor);

        videolist.setAdapter(resourceCursorAdapter);
        videolist.setOnItemClickListener(videogridlistener);*/
    }


    @Override
    public int setContentViewId() {
        return R.layout.main;
    }

    InterstitialAd interstitialAd;

    public void loadInterstitial() {
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getResources().getString(R.string.ads_interstitial));
        AdRequest adRequestInter = new AdRequest.Builder().build();

        // Load ads into Interstitial Ads
        interstitialAd.loadAd(adRequestInter);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if (mCountAds % 3 == 0) {
                    interstitialAd.show();
                }
            }
        });
    }

    @Override
    public void initView() {
        loadInterstitial();
        loadAds();
//        ActionBar actionBar= getSupportActionBar();
//        actionBar.setTitle("List Video");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List Video");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        rcvData.setHasFixedSize(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,2);

        rcvData.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void initData() {

    }

    Cursor videocursor;
    ListView videolist;

//    /** Called when the activity is first created. */
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setContentView(R.layout.main);
//    }

    private AdapterView.OnItemClickListener videogridlistener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView parent, View view, int position,
                                long id) {
            Cursor cursor = (Cursor) parent.getItemAtPosition(position);

            int fileNameIndex = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            String filename = cursor.getString(fileNameIndex);
            Intent intent = new Intent(WatchVideoActivity.this, SoundsActivity.class);
            intent.putExtra("videolink", filename);
            intent.putExtra("onlyview", true);
            startActivity(intent);
        }
    };

    private int getInt(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndexOrThrow(columnName);
        return cursor.getInt(index);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Subscribe
    public void chooseShowVideo(ChooseVideoEvent event){
        if(event!=null && event.getMedia()!=null){
            Intent intent = new Intent(WatchVideoActivity.this, SoundsActivity.class);
            intent.putExtra("videolink", event.getMedia().getUrl());
            intent.putExtra("onlyview", true);
            startActivity(intent);
        }
    }
}
