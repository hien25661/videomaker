package com.zodiac.videomaker.activities;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.zodiac.videomaker.customview.LayoutTutorialNew;
import com.zodiac.videomaker.helpers.event.FinishTutEvent;
import com.zodiac.videomaker.models.ImageManager;
import com.zodiac.videomaker.models.Picture;
import com.zodiac.videomusicmaker.R;

import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.OnClick;
import nl.changer.polypicker.Config;
import nl.changer.polypicker.ImagePickerActivity;

import static java.security.AccessController.getContext;

public class MenuActivity extends BaseActivity {
    @Bind(R.id.btn_create_video)
    ImageView btn_create_video;
    @Bind(R.id.btn_watch_video)
    ImageView btn_watch_video;
    public static final int INTENT_REQUEST_GET_IMAGES = 111;
    public static int mCountAds = 1;
    ImageManager imageManager;
    @Bind(R.id.adView)
    AdView adView;
    @Bind(R.id.tvMore)
    TextView tvMore;
    LayoutTutorialNew layoutTutorial;
    @Bind(R.id.viewTutorial)
    FrameLayout viewTutorial;
    @Override
    public int setContentViewId() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return R.layout.new_main;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
    boolean isRun;
    InterstitialAd interstitialAd;
    public void loadInterstitial() {
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getResources().getString(R.string.ads_interstitial));
        AdRequest adRequestInter = new AdRequest.Builder().build();

        // Load ads into Interstitial Ads
        interstitialAd.loadAd(adRequestInter);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if (mCountAds % 3 == 0) interstitialAd.show();
            }
        });
    }

    @Override
    public void initView() {
        imageManager = new ImageManager();
        Typeface font = Typeface.createFromAsset(getAssets(), "bebas-neue-regular.ttf");
        tvMore.setTypeface(font);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        loadInterstitial();
        showTutorialView(true);
    }

    @Override
    public void initData() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        // Permission Granted
                        if (isOpenLibrary && i == 0) {
                            getImages();
                        }
                    } else {
                        // Permission Denied
                        Toast.makeText(MenuActivity.this, STRING_DENY[i], Toast.LENGTH_SHORT)
                                .show();
                    }
                }

                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private final static String[] MULTI_PERMISSIONS = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private final static String[] STRING_DENY = new String[]{
            "Camera denied",
            "Read photo denied", "Write photo denied"
    };
    public static final int REQUEST_CODE_ASK_PERMISSIONS = 111;
    boolean isOpenLibrary;

    @OnClick(R.id.btn_create_video)
    public void openLirabry() {
        isOpenLibrary = true;
        Log.e("openLirabry", "openLirabry");
        int hasCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int hasRead = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int hasWrite = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (hasCamera != PackageManager.PERMISSION_GRANTED || hasRead != PackageManager.PERMISSION_GRANTED || hasWrite != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(MULTI_PERMISSIONS,
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }
        }
        getImages();
    }

    @OnClick(R.id.btn_watch_video)
    public void startWatch() {
        isOpenLibrary = false;
        int hasCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int hasRead = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int hasWrite = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (hasCamera != PackageManager.PERMISSION_GRANTED || hasRead != PackageManager.PERMISSION_GRANTED || hasWrite != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(MULTI_PERMISSIONS,
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }
        }
        startActivity(new Intent(this, WatchVideoActivity.class));
    }

    private void getImages() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        Config config = new Config.Builder()
                .setTabBackgroundColor(R.color.white)    // set tab background color. Default white.
                .setTabSelectionIndicatorColor(R.color.blue)
                .setCameraButtonColor(R.color.green)
                .build();
        ImagePickerActivity.setConfig(config);
        startActivityForResult(intent, INTENT_REQUEST_GET_IMAGES);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == RESULT_OK) {
            if (requestCode == INTENT_REQUEST_GET_IMAGES) {
                Parcelable[] parcelableUris = intent.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);

                if (parcelableUris == null) {
                    return;
                }

                // Java doesn't allow array casting, this is a little hack
                Uri[] uris = new Uri[parcelableUris.length];
                System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);

                if (uris != null) {
                    imageManager.clear();
                    for (int i = 0; i < uris.length; i++) {
                        Log.i("show uri", " uri: " + uris[i]);
//                        mMedia.add(uri);
                        Picture mPicture = new Picture(i, uris[i].toString());
                        imageManager.addUrl(mPicture);

                    }

//                    showMedia();
                }
                Intent mIntent = new Intent(MenuActivity.this, VideoMakerActivity.class);
                mIntent.putExtra("data", imageManager.converToString());
                startActivityForResult(mIntent, 222);

            } else if (requestCode == 222) {
                Log.e("onActivityResult: ", " " + interstitialAd.isLoaded());
//                if (interstitialAd.isLoaded()) {
//                    interstitialAd.show();
//                }
            }
        }
    }

    @OnClick(R.id.btn_rateus)
    public void rateUs() {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }

    }

    @OnClick(R.id.app1)
    public void openApp1() {
        final String appPackageName = "com.boxtimer365.pedometer"; // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }

    }

    @OnClick(R.id.app2)
    public void openApp2() {
        final String appPackageName = "com.boxtimer365.rainsounds"; // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @OnClick(R.id.app3)
    public void openApp3() {
        final String appPackageName = "com.boxtimer365.meditationsounds"; // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
    @OnClick(R.id.app4)
    public void openApp4() {
        final String appPackageName = "com.boxtimer365.ringstonecutter"; // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
    @OnClick(R.id.app5)
    public void openApp5() {
        final String appPackageName = "com.boxtimer365.videobox"; // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
    @OnClick(R.id.app6)
    public void openApp6() {
        final String appPackageName = "com.boxtimer365.bubble.shooter"; // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @OnClick(R.id.ll_more)
    public void openMore() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/developer?id=Zodiac_Entertainment")));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/developer?id=Zodiac_Entertainment")));
        }
    }

    private void showTutorialView(boolean isShow) {
        if (isShow) {
            viewTutorial.setVisibility(View.VISIBLE);
            viewTutorial.removeAllViews();
            layoutTutorial = new LayoutTutorialNew(this);
            viewTutorial.addView(layoutTutorial, FrameLayout.LayoutParams.MATCH_PARENT,FrameLayout.LayoutParams.MATCH_PARENT);
            viewTutorial.setVisibility(View.VISIBLE);
            viewTutorial.setAlpha(1);
            layoutTutorial.setUserTut();

        } else {
            viewTutorial.removeAllViews();
            viewTutorial.setVisibility(View.GONE);
        }

    }

    @Subscribe
    public void finishTutorial(FinishTutEvent event){
        if(event!=null && event.isFinish()){
            viewTutorial.setVisibility(View.GONE);
            viewTutorial.removeAllViews();
        }
    }

}
