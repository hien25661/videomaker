package com.zodiac.videomaker.customview;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;


import com.ramotion.paperonboarding.PaperOnboardingEngine;
import com.ramotion.paperonboarding.PaperOnboardingPage;
import com.ramotion.paperonboarding.listeners.PaperOnboardingOnChangeListener;
import com.ramotion.paperonboarding.listeners.PaperOnboardingOnRightOutListener;
import com.zodiac.videomaker.helpers.EventBusHelper;
import com.zodiac.videomaker.helpers.event.FinishTutEvent;
import com.zodiac.videomusicmaker.R;

import java.util.ArrayList;

import butterknife.ButterKnife;

/**
 * Created by hien.nv on 5/8/17.
 */

public class LayoutTutorialNew extends RelativeLayout {

    Context mContext;

    public LayoutTutorialNew(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public LayoutTutorialNew(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public LayoutTutorialNew(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        View v = inflate(getContext(), R.layout.layout_tutorial_new, this);
        ButterKnife.bind(this, v);
    }


    private String getString(int string_id) {
        return this.getResources().getString(string_id);
    }


    public void setUserTut(){
        PaperOnboardingEngine engine = new PaperOnboardingEngine(findViewById(R.id.onboardingRootView), getDataForOnboardingUser(), mContext);
        engine.isShowIconFirst = false;
        engine.setOnChangeListener(new PaperOnboardingOnChangeListener() {
            @Override
            public void onPageChanged(int oldElementIndex, int newElementIndex) {
                if(newElementIndex == 3){
                    EventBusHelper.post(new FinishTutEvent(true));
                }
            }
        });

        engine.setOnRightOutListener(new PaperOnboardingOnRightOutListener() {
            @Override
            public void onRightOut() {
                // Probably here will be your exit action
                EventBusHelper.post(new FinishTutEvent(true));
            }
        });
    }


    // Just example data for Onboarding
    private ArrayList<PaperOnboardingPage> getDataForOnboardingUser() {
        // prepare data
        PaperOnboardingPage scr1 = new PaperOnboardingPage(getString(R.string.tutorial_user_string1),
                getString(R.string.tutorial_user_string1),
                Color.TRANSPARENT,
                R.mipmap.ic_create,
                R.drawable.onboarding_pager_circle_icon);



        PaperOnboardingPage scr2 = new PaperOnboardingPage(getString(R.string.tutorial_user_string2),
                getString(R.string.tutorial_user_string2),
                Color.TRANSPARENT,
                R.mipmap.ic_list,
                R.drawable.onboarding_pager_circle_icon);

        PaperOnboardingPage scr3 = new PaperOnboardingPage(getString(R.string.tutorial_user_string3),
                getString(R.string.tutorial_user_string3),
                Color.TRANSPARENT,
                R.mipmap.ic_launcher,
                R.drawable.onboarding_pager_circle_icon);

        PaperOnboardingPage scr4 = new PaperOnboardingPage("",
                "",
                Color.TRANSPARENT,
                R.mipmap.tut1,
                R.drawable.onboarding_pager_circle_icon);


        ArrayList<PaperOnboardingPage> elements = new ArrayList<>();
        elements.add(scr1);
        elements.add(scr2);
        elements.add(scr3);
        elements.add(scr4);
        return elements;
    }
}
