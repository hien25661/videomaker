package com.zodiac.videomaker.customview;

/**
 * Created by Hien on 5/4/2017.
 */

public interface SuccessfullCallback {
    void success(Object... params);
}
