package com.zodiac.videomaker.customview;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.zodiac.videomusicmaker.R;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by hien.nv on 5/10/17.
 */

public class ProcessDialog extends Dialog {
    @Bind(R.id.tvMessage)
    TextView tvMessage;
    public ProcessDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.layout_progessbar);
        ButterKnife.bind(this);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }
    public void setMessage(String message){
        tvMessage.setVisibility(View.VISIBLE);
        tvMessage.setText(message);
    }

}
