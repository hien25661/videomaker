package com.zodiac.videomaker.utils;

import android.content.res.Resources;

/**
 * Created by Hien on 5/28/2017.
 */

public class Utils {
    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }
}
