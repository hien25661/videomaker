package com.zodiac.videomaker;

import android.app.Application;

import com.zodiac.videomaker.utils.SharedPrefUtils;

/**
 * Created by SF on 11/05/2016.
 */
public class VideoMakerApplication extends Application{
    private static VideoMakerApplication instance;
    private static SharedPrefUtils sharedPreferences;
    private static boolean mIsAppRunning;

    public VideoMakerApplication() {
        instance = this;
    }

    public static VideoMakerApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(new LifecycleCallback(new LifecycleCallback.OnAppRunningListener() {
            @Override
            public void isAppRunning(boolean isRunning) {
                mIsAppRunning = isRunning;
            }
        }));
        sharedPreferences = new SharedPrefUtils(getApplicationContext());

    }



    public static SharedPrefUtils getSharedPreferences() {
        return sharedPreferences;
    }
    public static boolean isAppRunning(){
        return mIsAppRunning;
    }
}
