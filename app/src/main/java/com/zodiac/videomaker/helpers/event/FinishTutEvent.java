package com.zodiac.videomaker.helpers.event;

/**
 * Created by hien.nv on 5/31/17.
 */

public class FinishTutEvent {
    private boolean isFinish;

    public FinishTutEvent(boolean isFinish) {
        this.isFinish = isFinish;
    }

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean finish) {
        isFinish = finish;
    }
}
