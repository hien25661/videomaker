package com.zodiac.videomaker.helpers;

import com.zodiac.videomaker.models.Media;

/**
 * Created by Hien on 5/28/2017.
 */

public class ChooseVideoEvent {
    Media media;

    public ChooseVideoEvent(Media media) {
        this.media = media;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }
}
