package com.zodiac.videomaker.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.zodiac.videomaker.customview.SuccessfullCallback;
import com.zodiac.videomaker.customview.VideoPlayer;
import com.zodiac.videomaker.helpers.AudioPicker;
import com.zodiac.videomaker.helpers.ChooseVideoEvent;
import com.zodiac.videomaker.helpers.EventBusHelper;
import com.zodiac.videomaker.models.Media;
import com.zodiac.videomaker.utils.Utils;
import com.zodiac.videomusicmaker.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Hien on 5/28/2017.
 */

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.VideoHolder> {
    Context mContext;
    ArrayList<Media> mediaArrayList = new ArrayList<>();

    public VideoListAdapter(ArrayList<Media> mediaArrayList) {
        this.mediaArrayList = mediaArrayList;
    }

    @Override
    public VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_video, parent, false);
        VideoHolder vh = new VideoHolder(v);
        mContext = v.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(final VideoHolder holder, int position) {
        if (position >= 0 && position < mediaArrayList.size()) {
            final Media media = mediaArrayList.get(position);
            if (media != null) {
                int videoNativeWidth = media.getWidth();
                int videoNativeHeight = media.getHeight();
                int screenWidth = (int)((float)Utils.getScreenWidth()/1.2f);
                if (videoNativeWidth != 0 && videoNativeHeight != 0) {
                    float ratio = (float) videoNativeHeight / videoNativeWidth;
                    int videoLayoutHeight = (int) (ratio * screenWidth);
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(screenWidth, videoLayoutHeight);
                    layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
                    holder.mediaVideo.setmWidth(screenWidth);
                    holder.mediaVideo.setmHeight(videoLayoutHeight);

                    holder.mediaVideo.setLayoutParams(layoutParams);
                    holder.overlayView.setLayoutParams(layoutParams);


                    holder.mediaVideo.setAvailableCallBack(new SuccessfullCallback() {
                        @Override
                        public void success(Object... params) {
                            holder.mediaVideo.playVideo();
                        }
                    });
                    holder.mediaVideo.loadVideo(media.getUrl(), media);
                }
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EventBusHelper.post(new ChooseVideoEvent(media));
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return mediaArrayList.size();
    }

    class VideoHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.player)
        VideoPlayer mediaVideo;
        @Bind(R.id.overlay)
        View overlayView;

        public VideoHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
